-- A short program for comparing a DNA motif to DNA Sequence
import System.IO

main = do
  handle <- openFile "input.txt" ReadMode
  input <- hGetContents handle

oneLineTime :: String -> String
oneLineTime input =
  let allLines = lines input
