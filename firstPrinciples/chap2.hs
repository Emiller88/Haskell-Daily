sayHello :: String -> IO()
sayHello x = putStrLn("Hello, " ++ x ++ "!")

triple x = x * 3

-- Comprehension Check 2
circleArea :: Float -> Float
circleArea x = 3.14 * x^2
-- 3
circleArea' :: Float -> Float
circleArea' x = pi * x^2
-- Excerises 2.7
x = 7
y = 10
f = x + y
